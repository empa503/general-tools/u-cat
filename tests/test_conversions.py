import pytest
import ucat

"""
cM: molar concentration [mol / m³]
cm: mass concentration [kg / m³]
cn: number concentration [molecules / m³]

xM: molar fraction [mol/mol]
xm: mass fraction [kg/kg]
"""


def test_xv_to_cN():
    result = ucat.convert_points(1e-9, 'm3/m3', 'm-3')
    assert result == pytest.approx(2.546917397567967e+16)


def test_cm_to_xv():
    result = ucat.convert_points(10.0, 'ug m-3', 'ppbv', molar_mass='NO2')
    assert result == pytest.approx(5.13957)


def test_xco2_co2_to_mass():
    """
    Convert XCO2 [ppmv] to mass columns for different surface pressure.
    """
    for p in [100e2, 500e2, 1000e2]:
        result = ucat.convert_columns(10.0, 'ppmv', 'kg m-2',
                                      molar_mass='CO2', p_surface=p)

        assert 1000e2 / p * result == pytest.approx(0.15491097871300876)


def test_kgs_to_Mtyr():
    """
    Convert kg/s to Mt/yr
    """
    x = 10
    result = ucat.convert_mass_per_time_unit(x, "kg/s", "Mt/yr")
    control = x * 60 * 60 * 24 * 365.25 / 1e3 / 1e6

    assert result == pytest.approx(control)

