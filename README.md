# u-cat: Unit conversion for atmospheric trace gases

This is a small Python package to convert units for the abundance and fluxes of 
atmospheric trace gases. The following conversions are supported:
- conversion of point measurements
- conversion of column measurements
- conversion of fluxes / emissions

Additionally, it includes several helper functions to parse, recognize and format units.

## Example
```python
import ucat

# convert 10 µg NO2 / m^3 to ppbv
ucat.convert_points(10.0, 'ug m-3', 'ppbv', molar_mass='NO2')

# convert 100 µmol/m² to molecules cm-2
ucat.convert_columns(100, 'umol m-2', 'cm-2', molar_mass='NO2')

# convert 10 kg/s to Mt/yr
ucat.convert_mass_per_time_unit(10, "kg/s", "Mt/yr")
```






